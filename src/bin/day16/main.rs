use itertools::Itertools;
use std::collections::{HashMap, HashSet};
use std::{env, fs};

fn main() {
    let arg = env::args().nth(1).unwrap();
    if arg != "1" && arg != "2" {
        println!("Only two puzzles daily!");
        return;
    }

    let records: Vec<String> = fs::read_to_string("./src/bin/day16/input")
        .unwrap()
        .split("\n\n")
        .map(|x| x.to_owned())
        .collect();

    let fields = records[0].split('\n');
    let my_ticket = records[1].split('\n').nth(1).unwrap();
    let nearby_tickets = records[2].split('\n').skip(1);

    let mut ranges: Vec<(usize, usize)> = vec![];
    for field in fields {
        for range in field.split(':').last().unwrap().split("or") {
            ranges.push(
                range
                    .trim()
                    .split('-')
                    .map(|e| e.parse().unwrap())
                    .collect_tuple()
                    .unwrap(),
            )
        }
    }

    let mut error_rate = 0usize;
    let mut possibilities: HashMap<usize, HashSet<usize>> = HashMap::new();
    for ticket in nearby_tickets {
        for (pos, value) in ticket
            .split(',')
            .map(|e| e.parse::<usize>().unwrap())
            .enumerate()
        {
            let mut field_choices: HashSet<usize> = HashSet::new();
            for (idx, (lower, upper)) in ranges.iter().enumerate() {
                if lower <= &value && &value <= upper {
                    field_choices.insert(idx / 2);
                }
            }

            if field_choices.is_empty() {
                error_rate += value;
            } else {
                possibilities
                    .entry(pos)
                    .and_modify(|e| *e = e.intersection(&field_choices).cloned().collect())
                    .or_insert(field_choices);
            }
        }
    }

    let mut untangle: Vec<(usize, HashSet<usize>)> = possibilities.into_iter().collect();
    untangle.sort_by_key(|(_, choices)| choices.len());

    let mut matching: HashMap<usize, usize> = HashMap::new();
    let mut assigned: HashSet<usize> = HashSet::new();

    for (pos, field_choices) in untangle {
        let resolved: Vec<usize> = field_choices.difference(&assigned).cloned().collect();
        assert_eq!(resolved.len(), 1);
        assigned.insert(resolved[0]);
        matching.insert(pos, resolved[0]);
    }

    let mut product = 1;
    for (idx, value) in my_ticket
        .split(',')
        .map(|e| e.parse::<usize>().unwrap())
        .enumerate()
    {
        if matching[&idx] < 6 {
            product *= value;
        }
    }

    if arg == "1" {
        println!("{}", error_rate)
    } else if arg == "2" {
        println!("{}", product)
    }
}
