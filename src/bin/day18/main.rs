use std::io::prelude::*;
use std::io::BufReader;
use std::{env, fs};

fn compute(expr: &[&str], precedence: bool) -> usize {
    match expr.iter().rposition(|&x| x == "(") {
        Some(l) => {
            let r = expr.iter().skip(l).position(|&x| x == ")").unwrap();
            let n = compute(&expr[l + 1..l + r], precedence).to_string();
            let mut new_expr = expr[..].to_vec();
            new_expr.drain(l..l + r);
            new_expr[l] = &n;
            compute(&new_expr, precedence)
        }
        None => {
            if precedence {
                match expr.iter().position(|&x| x == "+") {
                    Some(l) => {
                        let n = (expr[l - 1].parse::<usize>().unwrap()
                            + expr[l + 1].parse::<usize>().unwrap())
                        .to_string();
                        let mut new_expr = expr[..].to_vec();
                        new_expr.drain(l - 1..l + 1);
                        new_expr[l - 1] = &n;
                        compute(&new_expr, precedence)
                    }
                    None => {
                        let mut output = 1usize;
                        for a in expr {
                            if a != &"*" {
                                output *= a.parse::<usize>().unwrap();
                            }
                        }
                        output
                    }
                }
            } else if expr.len() == 1 {
                expr[0].parse::<usize>().unwrap()
            } else {
                let mut new_expr = expr[3..].to_vec();
                let mut n: usize = expr[0].parse().unwrap();
                if expr[1] == "+" {
                    n += expr[2].parse::<usize>().unwrap()
                } else if expr[1] == "*" {
                    n *= expr[2].parse::<usize>().unwrap()
                }
                let m = n.to_string();
                new_expr.insert(0, m.as_str());
                compute(&new_expr, precedence)
            }
        }
    }
}

fn main() {
    let arg = env::args().nth(1).unwrap();
    if arg != "1" && arg != "2" {
        println!("Only two puzzles daily!");
        return;
    }

    let mut precedence: bool = false;
    if arg == "2" {
        precedence = true
    }

    let records = BufReader::new(fs::File::open("./src/bin/day18/input").unwrap());
    println!(
        "{}",
        records
            .lines()
            .map(|line| {
                compute(
                    &line
                        .unwrap()
                        .replace("(", "( ")
                        .replace(")", " )")
                        .split(' ')
                        .collect::<Vec<&str>>(),
                    precedence,
                )
            })
            .sum::<usize>()
    )
}
