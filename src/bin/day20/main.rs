use itertools::Itertools;
use std::collections::{HashMap, HashSet};
use std::{env, fs};

fn flip(tile: &[String]) -> Vec<String> {
    let mut flipped: Vec<String> = vec![];
    for row in tile.iter() {
        flipped.push(row.chars().rev().collect());
    }
    flipped
}

fn rotate(tile: &[String]) -> Vec<String> {
    let mut rotated: Vec<String> = vec![];
    let size: usize = tile.len();
    for i in 0..size {
        let mut row = vec![];
        for j in 0..size {
            row.push(&tile[size - j - 1][i..i + 1])
        }
        rotated.push(row.join(""))
    }
    rotated
}

fn orientations(tile: &[String]) -> Vec<Vec<String>> {
    let mut oris: Vec<Vec<String>> = vec![];
    oris.push(tile.to_vec());
    for _ in 0..3 {
        oris.push(rotate(&oris[oris.len() - 1]))
    }
    oris.push(flip(&tile));
    for _ in 0..3 {
        oris.push(rotate(&oris[oris.len() - 1]))
    }
    oris
}

fn pound_indices(row: &str) -> HashSet<usize> {
    row.match_indices('#').map(|(idx, _)| idx).collect()
}

fn monster_indices(image: &[String], monster: &[&str]) -> HashSet<(usize, usize)> {
    let (mheight, mwidth): (usize, usize) = (monster.len(), monster[0].len());
    let imwidth: usize = image[0].len();
    let mpounds: Vec<HashSet<usize>> = monster.iter().map(|e| pound_indices(e)).collect();
    let mut mset = HashSet::new();

    for (row_n, window) in image.windows(mheight).enumerate() {
        for col_n in 0..=(imwidth - mwidth) {
            let found: bool = window.iter().enumerate().all(|(idx, row)| {
                mpounds[idx].is_subset(&pound_indices(&row[col_n..col_n + mwidth]))
            });
            if found {
                for (row_shift, iset) in mpounds.iter().enumerate() {
                    for col_shift in iset {
                        mset.insert((row_n + row_shift, col_n + col_shift));
                    }
                }
            }
        }
    }
    mset
}

fn separate(tile: &[String]) -> (Vec<String>, (String, String, String, String)) {
    let size = tile.len();
    let north = tile[0][..].to_owned();
    let south = tile[size - 1][..].to_owned();
    let mut west = vec![];
    let mut east = vec![];
    let mut imdata = vec![];

    for row in tile {
        let bits: Vec<char> = row.chars().collect();
        west.push(bits[0]);
        east.push(bits[size - 1]);
        imdata.push(bits[1..size - 1].iter().join(""));
    }

    let west = west.iter().join("");
    let east = east.iter().join("");

    (imdata[1..size - 1].to_vec(), (north, east, south, west))
}

fn arrange(
    imrow: &[(Option<String>, Option<String>)],
    tile_ids: &HashSet<(usize, usize)>,
    border_data: &HashMap<(usize, usize), (String, String, String, String)>,
) -> (bool, Vec<(usize, usize)>) {
    if tile_ids.is_empty() {
        return (true, vec![]);
    }

    let rowsize: usize = imrow.len();
    for tile in tile_ids.clone() {
        let lhs = &imrow[0];
        let rhs = &border_data[&tile];
        let match_north = lhs.0.is_none() || lhs.0.as_ref().unwrap() == &rhs.0;
        if match_north {
            let match_west = lhs.1.is_none() || lhs.1.as_ref().unwrap() == &rhs.3;
            if match_west {
                let mut new_imrow = imrow[1..].to_vec();
                new_imrow.push((Some(rhs.2.to_owned()), None));
                if (tile_ids.len() / 8) % rowsize != 1 {
                    new_imrow[0].1 = Some(rhs.1.to_owned());
                }

                let mut new_tile_ids = tile_ids.clone();
                for i in 0..8 {
                    new_tile_ids.remove(&(tile.0, i));
                }

                let try_arrange = arrange(&new_imrow, &new_tile_ids, border_data);
                if try_arrange.0 {
                    let mut arrangement = try_arrange.1;
                    arrangement.push(tile);
                    return (true, arrangement);
                }
            }
        }
    }
    (false, vec![])
}

fn main() {
    let arg = env::args().nth(1).unwrap();
    if arg != "1" && arg != "2" {
        println!("Only two puzzles daily!");
        return;
    }

    let records: Vec<String> = fs::read_to_string("./src/bin/day20/input")
        .unwrap()
        .split("\n\n")
        .map(|x| x.to_owned())
        .collect();

    let rowsize: usize = (records.len() as f64).sqrt() as usize;
    let tilesize: usize = &records[0].split('\n').count() - 1;
    let mut tile_ids = HashSet::new();
    let mut tile_data = HashMap::new();
    let mut border_data = HashMap::new();

    for info in records {
        let mut info = info.split('\n');
        let first = info.next().unwrap();
        let tid: usize = first[5..first.len() - 1].parse().unwrap();
        let tdata: Vec<String> = info.map(|e| e.to_owned()).collect();

        for (idx, ori) in orientations(&tdata).iter().cloned().enumerate() {
            let (imdata, border) = separate(&ori);
            let id = (tid, idx);
            tile_ids.insert(id);
            tile_data.insert(id, imdata);
            border_data.insert(id, border);
        }
    }

    let imrow: Vec<(Option<String>, Option<String>)> = vec![(None, None); rowsize];
    let (assembled, mut arrangement) = arrange(&imrow, &tile_ids, &border_data);
    assert!(assembled);

    arrangement.reverse();
    let mut image = vec![];
    for ids in arrangement.chunks(rowsize) {
        for row in 0..tilesize - 2 {
            image.push(ids.iter().map(|tile| &tile_data[tile][row]).join(""))
        }
    }

    let monster: [&str; 3] = [
        "                  # ",
        "#    ##    ##    ###",
        " #  #  #  #  #  #   ",
    ];

    if arg == "1" {
        let corners: [usize; 4] = [
            0,
            rowsize - 1,
            rowsize * (rowsize - 1),
            rowsize * rowsize - 1,
        ];
        let corner_product: usize = corners.iter().map(|idx| arrangement[*idx].0).product();
        println!("{}", corner_product);
    } else if arg == "2" {
        let pounds_total: usize = image.iter().map(|e| pound_indices(e).len()).sum();
        for ori in orientations(&image) {
            let pounds_monster = monster_indices(&ori, &monster).len();
            if pounds_monster != 0 {
                println!("{}", pounds_total - pounds_monster);
                break;
            }
        }
    }
}
