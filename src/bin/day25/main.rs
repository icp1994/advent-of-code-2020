use std::{env, fs};

fn main() {
    let arg = env::args().nth(1).unwrap();
    if arg != "1" {
        println!("Surprise!");
        return;
    }

    let records: Vec<usize> = fs::read_to_string("./src/bin/day25/input")
        .unwrap()
        .split('\n')
        .map(|x| x.parse().unwrap())
        .collect();

    let (card_pk, door_pk) = (records[0], records[1]);
    let mut value = 1usize;
    let mut card_loop_size = 0usize;
    while value != card_pk {
        value = (value * 7) % 20201227;
        card_loop_size += 1;
    }

    let mut encrypt_door_pk = 1;
    for _ in 0..card_loop_size {
        encrypt_door_pk = (encrypt_door_pk * door_pk) % 20201227;
    }
    println!("{}", encrypt_door_pk);

    value = 1;
    let mut door_loop_size = 0usize;
    while value != door_pk {
        value = (value * 7) % 20201227;
        door_loop_size += 1;
    }

    let mut encrypt_card_pk = 1;
    for _ in 0..door_loop_size {
        encrypt_card_pk = (encrypt_card_pk * card_pk) % 20201227;
    }
    assert_eq!(encrypt_door_pk, encrypt_card_pk);
}
