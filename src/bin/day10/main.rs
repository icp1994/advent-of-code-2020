use std::collections::{HashMap, HashSet};
use std::{env, fs};

fn valid_arrs(
    jolts: &HashSet<usize>,
    start_jolt: usize,
    end_jolt: usize,
    cache: &mut HashMap<usize, usize>,
) -> usize {
    match cache.get(&start_jolt) {
        Some(output) => *output,
        None => {
            if start_jolt == end_jolt {
                return 1;
            }
            let mut output = 0usize;
            for jolt in start_jolt + 1..=start_jolt + 3 {
                if jolts.contains(&jolt) {
                    let new_arrs = valid_arrs(jolts, jolt, end_jolt, cache);
                    cache.insert(jolt, new_arrs);
                    output += new_arrs;
                }
            }
            output
        }
    }
}

fn main() {
    let mut records: Vec<usize> = fs::read_to_string("./src/bin/day10/input")
        .unwrap()
        .split('\n')
        .map(|num| num.parse().unwrap())
        .collect();
    records.sort_unstable();
    let jolts: HashSet<usize> = records.iter().cloned().collect();

    match env::args().nth(1).unwrap().as_str() {
        "1" => {
            let mut diff1 = (records[0] == 1) as usize;
            let mut diff3 = 1usize;
            for i in 1..records.len() {
                if records[i] - records[i - 1] == 1 {
                    diff1 += 1
                } else if records[i] - records[i - 1] == 3 {
                    diff3 += 1
                }
            }
            println!("{}", diff1 * diff3);
        }
        "2" => {
            println!(
                "{}",
                valid_arrs(&jolts, 0, records[records.len() - 1], &mut HashMap::new())
            );
        }
        _ => println!("Only two puzzles daily!"),
    }
}
