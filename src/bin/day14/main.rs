use itertools::repeat_n;
use itertools::Itertools;
use std::collections::HashMap;
use std::io::prelude::*;
use std::io::BufReader;
use std::{env, fs};

fn main() {
    let arg = env::args().nth(1).unwrap();
    if arg != "1" && arg != "2" {
        println!("Only two puzzles daily!");
        return;
    }

    let records = BufReader::new(fs::File::open("./src/bin/day14/input").unwrap());
    let mut memeory: HashMap<usize, usize> = HashMap::new();
    let mut mask: Vec<char> = vec![];
    let mut bin_strs = HashMap::new();

    for line in records.lines() {
        let line = line.unwrap();
        if &line[..4] == "mask" {
            mask = line[7..].chars().collect();
            continue;
        }

        let addr = &line[line.find('[').unwrap() + 1..line.find(']').unwrap()];
        let value = &line[line.find('=').unwrap() + 2..];

        if arg == "1" {
            let mut result: Vec<char> = format!("{:036b}", value.parse::<usize>().unwrap())
                .chars()
                .collect();

            for idx in 0..36 {
                if mask[idx] != 'X' {
                    result[idx] = mask[idx]
                }
            }

            memeory.insert(
                addr.parse::<usize>().unwrap(),
                usize::from_str_radix(&result.iter().collect::<String>(), 2).unwrap(),
            );
        } else if arg == "2" {
            let mut result: Vec<char> = format!("{:036b}", addr.parse::<usize>().unwrap())
                .chars()
                .collect();
            let mut floating_indices: Vec<usize> = vec![];

            for idx in 0..36 {
                match mask[idx] {
                    '1' => result[idx] = '1',
                    'X' => {
                        result[idx] = 'X';
                        floating_indices.push(idx)
                    }
                    _ => (),
                }
            }

            if floating_indices.is_empty() {
                memeory.insert(
                    usize::from_str_radix(&result.iter().collect::<String>(), 2).unwrap(),
                    value.parse::<usize>().unwrap(),
                );
                continue;
            }

            bin_strs.entry(floating_indices.len()).or_insert_with(|| {
                repeat_n("01".chars(), floating_indices.len()).multi_cartesian_product()
            });

            for floating_bits in bin_strs[&floating_indices.len()].clone() {
                let mut mem = result.clone();
                for (idx, bit) in floating_bits.iter().enumerate() {
                    mem[floating_indices[idx]] = *bit;
                }
                memeory.insert(
                    usize::from_str_radix(&mem.iter().collect::<String>(), 2).unwrap(),
                    value.parse::<usize>().unwrap(),
                );
            }
        }
    }
    println!("{}", memeory.values().sum::<usize>())
}
