use lazy_static::lazy_static;
use regex::Regex;
use std::collections::HashMap;
use std::{env, fs};

fn transform(passport: &str) -> Option<String> {
    let mut output = Vec::new();
    let mut records = HashMap::new();
    for data in passport.split_whitespace() {
        let (key, val) = data.split_at(data.find(':').unwrap());
        records.insert(key, val);
    }
    for field in "byr iyr eyr hgt hcl ecl pid".split(' ') {
        match records.get(field) {
            Some(value) => output.push(*value),
            None => return None,
        }
    }
    Some(output.join(""))
}

fn main() {
    let records: Vec<String> = fs::read_to_string("./src/bin/day04/input")
        .unwrap()
        .split("\n\n")
        .filter_map(|record| transform(record))
        .collect();
    let args: Vec<String> = env::args().collect();

    match args[1].as_str() {
        "1" => println!("{}", records.len()),
        "2" => {
            lazy_static! {
                static ref RE: Regex = Regex::new(
                    r"(?x)
                    :(?P<byr>\d{4})
                    :(?P<iyr>\d{4})
                    :(?P<eyr>\d{4})
                    :(?P<hgt>[1-9]\d+)(?P<unit>(cm|in))
                    :(\#[0-9a-f]{6})
                    :(amb|blu|brn|gry|grn|hzl|oth)
                    :(\d{9})$"
                )
                .unwrap();
            }

            let mut output = 0usize;
            for record in records {
                if let Some(caps) = RE.captures(&record) {
                    if (2020..=2030).contains(&caps["eyr"].parse::<u16>().unwrap())
                        && (2010..=2020).contains(&caps["iyr"].parse::<u16>().unwrap())
                        && (1920..=2002).contains(&caps["byr"].parse::<u16>().unwrap())
                        && (((150..=193).contains(&caps["hgt"].parse::<u16>().unwrap())
                            && (&caps["unit"] == "cm"))
                            || ((59..=76).contains(&caps["hgt"].parse::<u16>().unwrap())
                                && (&caps["unit"] == "in")))
                    {
                        output += 1
                    }
                }
            }
            println!("{}", output)
        }
        _ => println!("Only two puzzles daily!"),
    }
}
