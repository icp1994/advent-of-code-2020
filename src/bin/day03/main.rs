use std::{env, fs};

fn trees_in_slope(map: &[Vec<bool>], skip_x: usize, skip_y: usize) -> usize {
    let (vlimit, hlimit) = (map.len(), map[0].len());
    let (mut trees, mut row, mut col) = (0usize, 0usize, 0usize);
    while row < vlimit {
        if map[row][col] {
            trees += 1
        }
        row += skip_y;
        col = (col + skip_x) % hlimit;
    }
    trees
}

fn main() {
    let records: Vec<Vec<bool>> = fs::read_to_string("./src/bin/day03/input")
        .unwrap()
        .split('\n')
        .map(|row| row.chars().map(|c| c == '#').collect())
        .collect();
    let args: Vec<String> = env::args().collect();

    match args[1].as_str() {
        "1" => println!("{}", trees_in_slope(&records, 3, 1)),
        "2" => {
            let mut output = trees_in_slope(&records, 1, 2);
            for idx in (1..=7).step_by(2) {
                output *= trees_in_slope(&records, idx, 1)
            }
            println!("{}", output)
        }
        _ => println!("Only two puzzles daily!"),
    }
}
