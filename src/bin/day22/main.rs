use itertools::Itertools;
use std::collections::HashSet;
use std::{env, fs};

fn main() {
    let arg = env::args().nth(1).unwrap();
    if arg != "1" && arg != "2" {
        println!("Only two puzzles daily!");
        return;
    }

    let (mut player_1, mut player_2): (Vec<usize>, Vec<usize>) =
        fs::read_to_string("./src/bin/day22/input")
            .unwrap()
            .split("\n\n")
            .map(|player| {
                player
                    .split('\n')
                    .skip(1)
                    .map(|card| card.parse().unwrap())
                    .collect()
            })
            .collect_tuple()
            .unwrap();

    fn combat(
        player_1: &mut Vec<usize>,
        player_2: &mut Vec<usize>,
        recursive: bool,
        configs: Option<HashSet<(Vec<usize>, Vec<usize>)>>,
    ) -> usize {
        let mut configs: HashSet<(Vec<usize>, Vec<usize>)> = configs.unwrap_or_default();
        loop {
            if configs.contains(&(player_1.clone(), player_2.clone())) {
                return 1;
            }

            configs.insert((player_1.clone(), player_2.clone()));
            let (fst, snd) = (player_1.remove(0), player_2.remove(0));
            let mut winner = 2usize;

            if recursive && player_1.len() >= fst && player_2.len() >= snd {
                winner = combat(
                    &mut player_1[..fst].to_vec(),
                    &mut player_2[..snd].to_vec(),
                    true,
                    None,
                );
            } else if fst > snd {
                winner = 1;
            }

            if winner == 1 {
                player_1.push(fst);
                player_1.push(snd);
                if player_2.is_empty() {
                    return 1;
                }
            } else {
                player_2.push(snd);
                player_2.push(fst);
                if player_1.is_empty() {
                    return 2;
                }
            }
        }
    }

    let mut winner = 0usize;
    if arg == "1" {
        winner = combat(&mut player_1, &mut player_2, false, None);
    } else if arg == "2" {
        winner = combat(&mut player_1, &mut player_2, true, None);
    }

    let score: usize = [player_1, player_2][winner - 1]
        .iter()
        .rev()
        .enumerate()
        .map(|(pos, value)| (pos + 1) * value)
        .sum();
    println!("{}", score);
}
