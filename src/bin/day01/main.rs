use std::collections::HashSet;
use std::{env, fs};

fn twosum(numbers: Vec<isize>, hash: HashSet<isize>) -> Option<isize> {
    for num in numbers.iter() {
        if hash.contains(num) {
            return Some(num * (2020 - num));
        }
    }
    None
}

fn threesum(numbers: Vec<isize>, hash: HashSet<isize>) -> Option<isize> {
    for (idx, num1) in numbers.iter().enumerate() {
        for num2 in numbers[idx + 1..].iter() {
            if hash.contains(&(num1 + num2)) {
                return Some(num1 * num2 * (2020 - num1 - num2));
            }
        }
    }
    None
}

fn main() {
    let numbers: Vec<isize> = fs::read_to_string("./src/bin/day01/input")
        .unwrap()
        .split('\n')
        .map(|num| num.parse().unwrap())
        .collect();
    let mut differences = HashSet::new();
    for num in numbers.iter() {
        differences.insert(2020 - num);
    }
    let args: Vec<String> = env::args().collect();
    match args[1].as_str() {
        "1" => println!("{}", twosum(numbers, differences).unwrap_or_default()),
        "2" => println!("{}", threesum(numbers, differences).unwrap_or_default()),
        _ => println!("Only two puzzles daily!"),
    }
}
