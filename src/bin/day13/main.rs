use std::{env, fs};

fn rem_chinese(modulos: Vec<(isize, isize)>) -> isize {
    let mut output = 0isize;
    let product: isize = modulos.iter().map(|x| x.1).product();
    for (rem, div) in modulos {
        let trunc = product / div;
        let mut inv = 1isize;
        loop {
            if (trunc * inv).rem_euclid(div) == 1 {
                break;
            }
            inv += 1;
        }
        output += rem * trunc * inv
    }
    output.rem_euclid(product)
}

fn main() {
    let arg = env::args().nth(1).unwrap();
    if arg != "1" && arg != "2" {
        println!("Only two puzzles daily!");
        return;
    }

    let records: Vec<String> = fs::read_to_string("./src/bin/day13/input")
        .unwrap()
        .split('\n')
        .map(|x| x.to_owned())
        .collect();

    let buses: Vec<(isize, isize)> = records[1]
        .split(',')
        .enumerate()
        .filter(|e| e.1 != "x")
        .map(|e| {
            let n = e.1.parse::<isize>().unwrap();
            ((0 - e.0 as isize).rem_euclid(n), n)
        })
        .collect();

    if arg == "1" {
        let earliest: isize = records[0].parse().unwrap();
        let mut waiting = 0;
        'outer: loop {
            for bus in buses.iter() {
                if (earliest + waiting) % bus.1 == 0 {
                    println!("{}", bus.1 * waiting);
                    break 'outer;
                }
            }
            waiting += 1
        }
    } else if arg == "2" {
        println!("{}", rem_chinese(buses))
    }
}
