use std::collections::HashMap;
use std::io::prelude::*;
use std::io::BufReader;
use std::iter;
use std::{env, fs};

fn update_position(action: char, value: usize, position: &mut HashMap<char, usize>) {
    let opp: HashMap<char, char> = "ESWN".chars().zip("WNES".chars()).collect();
    let mut opp_value = position[&opp[&action]];
    if opp_value >= value {
        opp_value -= value
    } else if opp_value > 0 {
        position.insert(action, value - opp_value);
        opp_value = 0
    } else {
        position.entry(action).and_modify(|e| *e += value);
    }
    position.insert(opp[&action], opp_value);
}

fn update_waypoint(action: char, value: usize, waypoint: &mut HashMap<char, usize>) {
    let mut new_wp: HashMap<char, usize> = "ESWN".chars().zip(iter::repeat(0)).collect();
    let keys: Vec<char> = waypoint
        .keys()
        .filter(|k| waypoint[k] != 0)
        .cloned()
        .collect();

    for key in keys {
        let new_key: char;
        if action == 'R' {
            let pos = ("ESWN".find(key).unwrap() + value / 90) % 4;
            new_key = vec!['E', 'S', 'W', 'N'][pos as usize];
        } else {
            let pos = ("ESWN".find(key).unwrap() as isize - value as isize / 90).rem_euclid(4);
            new_key = vec!['E', 'S', 'W', 'N'][pos as usize];
        }
        if new_key != key {
            new_wp.insert(new_key, waypoint[&key]);
        }
    }
    *waypoint = new_wp;
}

fn main() {
    let arg = env::args().nth(1).unwrap();
    if (arg != "1") & (arg != "2") {
        println!("Only two puzzles daily!");
        return;
    }

    let records = BufReader::new(fs::File::open("./src/bin/day12/input").unwrap());
    let mut position: HashMap<char, usize> = "ESWN".chars().zip(iter::repeat(0)).collect();
    let mut waypoint: HashMap<char, usize> = "ESWN".chars().zip(iter::repeat(0)).collect();
    let mut face = 'E';

    if arg == "2" {
        waypoint.insert('E', 10);
        waypoint.insert('N', 1);
    }

    for line in records.lines() {
        let line = line.unwrap();
        let mut command = line.chars();
        let action = command.next().unwrap();
        let value: usize = command.collect::<String>().parse().unwrap();

        if arg == "1" {
            match action {
                'R' => {
                    let pos = ("ESWN".find(face).unwrap() + value / 90) % 4;
                    face = vec!['E', 'S', 'W', 'N'][pos]
                }
                'L' => {
                    let pos =
                        ("ESWN".find(face).unwrap() as isize - value as isize / 90).rem_euclid(4);
                    face = vec!['E', 'S', 'W', 'N'][pos as usize]
                }
                'F' => update_position(face, value, &mut position),
                _ => update_position(action, value, &mut position),
            }
        } else {
            match action {
                'R' | 'L' => update_waypoint(action, value, &mut waypoint),
                'F' => {
                    for key in waypoint.keys().cloned() {
                        if waypoint[&key] != 0 {
                            update_position(key, value * waypoint[&key], &mut position)
                        }
                    }
                }
                _ => update_position(action, value, &mut waypoint),
            }
        }
    }
    println!("{}", position.values().sum::<usize>());
}
