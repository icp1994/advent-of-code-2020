use std::collections::HashMap;
use std::{env, fs};

fn main() {
    let arg = env::args().nth(1).unwrap();
    if arg != "1" && arg != "2" {
        println!("Only two puzzles daily!");
        return;
    }

    let mut cups: Vec<usize> = fs::read_to_string("./src/bin/day23/input")
        .unwrap()
        .chars()
        .map(|e| e.to_string().parse().unwrap())
        .collect();

    let mut cupcap: usize = *cups.iter().max().unwrap();
    let mut moves: usize = 100;

    if arg == "2" {
        for cup in cupcap + 1..=1_000_000 {
            cups.push(cup)
        }
        cupcap = 1_000_000;
        moves = 10_000_000;
    }

    let mut circle = HashMap::new();
    for i in 0..=cups.len() - 2 {
        circle.insert(cups[i], cups[i + 1]);
    }
    circle.insert(cups[cups.len() - 1], cups[0]);

    let mut current = cups[0];
    for _ in 1..=moves {
        let mut selected = vec![];
        let mut t = current;
        for _ in 1..=3 {
            t = circle[&t];
            selected.push(t);
        }

        let mut destination = current - 1;
        while destination == 0 || selected.contains(&destination) {
            if destination == 0 {
                destination = cupcap;
                continue;
            }
            destination -= 1;
        }

        circle.insert(current, circle[&t]);
        let temp = circle[&destination];
        circle.insert(destination, selected[0]);
        circle.insert(t, temp);
        current = circle[&current];
    }

    if arg == "1" {
        let mut after_one = vec![];
        let mut cup = 1;
        for _ in 1..=cups.len() - 1 {
            cup = circle[&cup];
            after_one.push(cup.to_string())
        }
        println!("{}", after_one.join(""))
    } else if arg == "2" {
        println!("{}", circle[&1] * circle[&circle[&1]])
    }
}
