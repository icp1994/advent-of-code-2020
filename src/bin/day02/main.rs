use lazy_static::lazy_static;
use regex::{CaptureMatches, Regex};
use std::{env, fs};

fn policy_occurences(captures: CaptureMatches) -> usize {
    let mut valid_passes = 0usize;
    for capture in captures {
        let lbound: usize = capture["lo"].parse().unwrap();
        let ubound: usize = capture["hi"].parse().unwrap();
        let occurences: usize = capture["pass"].matches(&capture["key"]).count();
        if (lbound <= occurences) && (occurences <= ubound) {
            valid_passes += 1;
        }
    }
    valid_passes
}

fn policy_positions(captures: CaptureMatches) -> usize {
    let mut valid_passes = 0usize;
    for capture in captures {
        let pos_1: usize = capture["lo"].parse().unwrap();
        let pos_2: usize = capture["hi"].parse().unwrap();
        if (capture["pass"][pos_1 - 1..pos_1] == capture["key"])
            ^ (capture["pass"][pos_2 - 1..pos_2] == capture["key"])
        {
            valid_passes += 1;
        }
    }
    valid_passes
}

fn main() {
    lazy_static! {
        static ref RE: Regex =
            Regex::new(r"(?P<lo>\d+)-(?P<hi>\d+) (?P<key>[\S]): (?P<pass>[\S]+)").unwrap();
    }

    let records = fs::read_to_string("./src/bin/day02/input").unwrap();
    let captures = RE.captures_iter(&records);
    let args: Vec<String> = env::args().collect();

    match args[1].as_str() {
        "1" => println!("{}", policy_occurences(captures)),
        "2" => println!("{}", policy_positions(captures)),
        _ => println!("Only two puzzles daily!"),
    }
}
