use itertools::Itertools;
use std::collections::{HashMap, HashSet};
use std::{env, fs};

fn main() {
    let arg = env::args().nth(1).unwrap();
    if arg != "1" && arg != "2" {
        println!("Only two puzzles daily!");
        return;
    }

    let records: Vec<String> = fs::read_to_string("./src/bin/day21/input")
        .unwrap()
        .split('\n')
        .map(|x| x.to_owned())
        .collect();

    let mut counter: HashMap<&str, usize> = HashMap::new();
    let mut maybes: HashMap<&str, HashSet<&str>> = HashMap::new();

    for food in &records {
        let (lhs, rhs) = food.split(" (contains ").collect_tuple().unwrap();
        let ingredients: HashSet<&str> = lhs.split(' ').collect();

        for ingredient in &ingredients {
            *counter.entry(ingredient).or_insert(0) += 1;
        }

        for allergen in rhs.split(' ').map(|e| &e[..e.len() - 1]) {
            maybes
                .entry(allergen)
                .and_modify(|e| *e = &*e & &ingredients)
                .or_insert_with(|| ingredients.clone());
        }
    }

    let mut allergens: HashSet<&str> = maybes.keys().cloned().collect();
    let mut matching: HashMap<&str, &str> = HashMap::new();
    let mut assigned: HashSet<&str> = HashSet::new();

    while !allergens.is_empty() {
        for allergen in allergens.clone().iter() {
            let mut choices: HashSet<&str> = &maybes[allergen] - &assigned;
            if choices.len() == 1 {
                let ingredient = choices.drain().next().unwrap();
                matching.insert(allergen, ingredient);
                assigned.insert(ingredient);
                allergens.remove(allergen);
            }
        }
    }

    let mut occurences: usize = counter.values().sum();
    let mut dangerous: Vec<&str> = vec![];
    let mut allergens: Vec<&str> = matching.keys().cloned().collect();
    allergens.sort_unstable();

    for allergen in allergens {
        let ingredient = matching[allergen];
        occurences -= counter[ingredient];
        dangerous.push(ingredient);
    }

    if arg == "1" {
        println!("{}", occurences)
    } else if arg == "2" {
        println!("{}", dangerous.iter().join(","))
    }
}
