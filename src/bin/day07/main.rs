use lazy_static::lazy_static;
use regex::Regex;
use std::collections::{HashMap, HashSet};
use std::{env, fs};

fn nb_predecessors(child: &str, parents: &HashMap<String, HashSet<&str>>) -> usize {
    let mut generation = parents.get(child).unwrap().clone();
    let mut predecessors: HashSet<&str> = HashSet::new();

    while !generation.is_empty() {
        predecessors = &predecessors | &generation;
        let mut finnish: HashSet<&str> = HashSet::new();
        for parent in generation.iter() {
            finnish = &finnish | parents.get(parent.to_owned()).unwrap_or(&HashSet::new());
        }
        generation = &finnish - &predecessors;
    }
    predecessors.len()
}

fn nb_successors(parent: &str, children: &HashMap<String, Vec<(&str, usize)>>) -> usize {
    if let Some(successors) = children.get(parent) {
        return successors
            .iter()
            .map(|successor| (nb_successors(successor.0, children) + 1) * successor.1)
            .sum();
    }
    0
}

fn main() {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"(\d+)?\s*?(\w+\s\w+) bag").unwrap();
    }

    let records = fs::read_to_string("./src/bin/day07/input").unwrap();
    let mut parents: HashMap<String, HashSet<&str>> = HashMap::new();
    let mut children: HashMap<String, Vec<(&str, usize)>> = HashMap::new();

    for rule in records.split('\n') {
        let mut caps = RE.captures_iter(rule);
        let parent = caps.next().unwrap().get(2).unwrap().as_str();

        for child in caps {
            if child.get(1).is_some() {
                let curr_child = children.entry(parent.to_owned()).or_insert(vec![]);
                curr_child.push((
                    child.get(2).unwrap().as_str(),
                    child.get(1).unwrap().as_str().parse().unwrap(),
                ));
                let blank: HashSet<&str> = HashSet::new();
                let curr_parent = parents.entry(child[2].to_owned()).or_insert(blank);
                curr_parent.insert(parent);
            }
        }
    }

    let args: Vec<String> = env::args().collect();

    match args[1].as_str() {
        "1" => println!("{}", nb_predecessors("shiny gold", &parents)),
        "2" => println!("{}", nb_successors("shiny gold", &children)),
        _ => println!("Only two puzzles daily!"),
    }
}
