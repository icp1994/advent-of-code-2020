use std::collections::BinaryHeap;
use std::io::prelude::*;
use std::io::BufReader;
use std::{env, fs};

fn locate_seat(boarding_pass: &str) -> usize {
    let mut id = 0;
    for c in boarding_pass.chars() {
        id <<= 1;
        if c == 'B' || c == 'R' {
            id |= 1;
        }
    }
    id
}

fn main() {
    let records = BufReader::new(fs::File::open("./src/bin/day05/input").unwrap());
    let mut seat_ids = BinaryHeap::new();
    for boarding_pass in records.lines() {
        seat_ids.push(locate_seat(&boarding_pass.unwrap()));
    }
    let args: Vec<String> = env::args().collect();

    match args[1].as_str() {
        "1" => println!("{}", seat_ids.peek().unwrap()),
        "2" => {
            let seats = seat_ids.into_sorted_vec();
            for (idx, id) in seats[1..].iter().enumerate() {
                if id - seats[idx] == 2 {
                    println!("{}", id - 1);
                    return;
                }
            }
        }
        _ => println!("Only two puzzles daily!"),
    }
}
