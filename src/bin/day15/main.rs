use std::collections::HashMap;
use std::{env, fs};

fn main() {
    let arg = env::args().nth(1).unwrap();
    if arg != "1" && arg != "2" {
        println!("Only two puzzles daily!");
        return;
    }

    let records: Vec<usize> = fs::read_to_string("./src/bin/day15/input")
        .unwrap()
        .split(',')
        .map(|x| x.parse().unwrap())
        .collect();

    let mut last: usize = records[records.len() - 1];
    let mut positions: HashMap<usize, usize> = HashMap::new();
    for (idx, num) in records[..records.len() - 1].iter().cloned().enumerate() {
        positions.insert(num, idx + 1);
    }

    let mut target = 0usize;
    if arg == "1" {
        target = 2020
    } else if arg == "2" {
        target = 30000000
    }

    let mut turn: usize = records.len() + 1;
    while turn <= target {
        positions
            .entry(last)
            .and_modify(|e| {
                last = turn - 1 - *e;
                *e = turn - 1;
            })
            .or_insert_with(|| {
                last = 0;
                turn - 1
            });
        turn += 1
    }
    println!("{}", last);
}
