use itertools::iproduct;
use std::collections::HashMap;
use std::{env, fs};

fn main() {
    let arg = env::args().nth(1).unwrap();
    if arg != "1" && arg != "2" {
        println!("Only two puzzles daily!");
        return;
    }
    const N: usize = 6 + 1;

    let records: Vec<String> = fs::read_to_string("./src/bin/day17/input")
        .unwrap()
        .split('\n')
        .map(|x| x.to_owned())
        .collect();

    let (r, c) = (records.len(), records[0].len());
    if arg == "1" {
        let mut positions: HashMap<(usize, usize, usize), char> = HashMap::new();
        for (i, row) in records.iter().enumerate() {
            for (j, state) in row.chars().enumerate() {
                positions.insert((i + N, j + N, N), state);
            }
        }

        for t in 1..N {
            let mut to_activate: Vec<(usize, usize, usize)> = vec![];
            let mut to_inactivate: Vec<(usize, usize, usize)> = vec![];
            for (x, y, z) in iproduct!(N - t..N + r + t, N - t..N + c + t, N - t..=N + t) {
                let mut active = 0;
                for nbor in iproduct!(x - 1..x + 2, y - 1..y + 2, z - 1..z + 2) {
                    if nbor != (x, y, z) && positions.get(&nbor) == Some(&'#') {
                        active += 1;
                    }
                }

                positions.entry((x, y, z)).or_insert('.');
                if positions[&(x, y, z)] == '#' && !(2..4).contains(&active) {
                    to_inactivate.push((x, y, z))
                }
                if positions[&(x, y, z)] == '.' && active == 3 {
                    to_activate.push((x, y, z))
                }
            }

            for p in to_activate {
                positions.insert(p, '#');
            }
            for p in to_inactivate {
                positions.insert(p, '.');
            }
        }
        println!("{}", positions.iter().filter(|e| e.1 == &'#').count());
    } else if arg == "2" {
        let mut positions: HashMap<(usize, usize, usize, usize), char> = HashMap::new();
        for (i, row) in records.iter().enumerate() {
            for (j, state) in row.chars().enumerate() {
                positions.insert((i + N, j + N, N, N), state);
            }
        }

        for t in 1..N {
            let mut to_activate: Vec<(usize, usize, usize, usize)> = vec![];
            let mut to_inactivate: Vec<(usize, usize, usize, usize)> = vec![];
            for (x, y, z, w) in iproduct!(
                N - t..N + r + t,
                N - t..N + c + t,
                N - t..=N + t,
                N - t..=N + t
            ) {
                let mut active = 0;
                for nbor in iproduct!(x - 1..x + 2, y - 1..y + 2, z - 1..z + 2, w - 1..w + 2) {
                    if nbor != (x, y, z, w) && positions.get(&nbor) == Some(&'#') {
                        active += 1;
                    }
                }

                positions.entry((x, y, z, w)).or_insert('.');
                if positions[&(x, y, z, w)] == '#' && !(2..4).contains(&active) {
                    to_inactivate.push((x, y, z, w))
                }
                if positions[&(x, y, z, w)] == '.' && active == 3 {
                    to_activate.push((x, y, z, w))
                }
            }

            for p in to_activate {
                positions.insert(p, '#');
            }
            for p in to_inactivate {
                positions.insert(p, '.');
            }
        }
        println!("{}", positions.iter().filter(|e| e.1 == &'#').count());
    }
}
