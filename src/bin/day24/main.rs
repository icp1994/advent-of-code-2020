use std::collections::{HashMap, HashSet};
use std::{env, fs};

fn neighbours(tile: (isize, isize), with_self: bool) -> HashSet<(isize, isize)> {
    let neighbours = [
        (tile.0, tile.1),
        (tile.0 + 2, tile.1),
        (tile.0 - 2, tile.1),
        (tile.0 + 1, tile.1 + 1),
        (tile.0 + 1, tile.1 - 1),
        (tile.0 - 1, tile.1 + 1),
        (tile.0 - 1, tile.1 - 1),
    ];
    let start = usize::from(!with_self);
    neighbours[start..].iter().cloned().collect()
}

fn main() {
    let arg = env::args().nth(1).unwrap();
    if arg != "1" && arg != "2" {
        println!("Only two puzzles daily!");
        return;
    }

    let records: Vec<String> = fs::read_to_string("./src/bin/day24/input")
        .unwrap()
        .split('\n')
        .map(|x| x.to_owned())
        .collect();

    let mut tiles_hit: HashMap<(isize, isize), usize> = HashMap::new();
    for directions in records {
        let mut tile_at = (0isize, 0isize);
        let eswn: Vec<char> = directions.chars().collect();
        let mut cursor = 0usize;

        while cursor < eswn.len() {
            if eswn[cursor] == 'e' {
                tile_at.0 += 2;
                cursor += 1;
            } else if eswn[cursor] == 'w' {
                tile_at.0 -= 2;
                cursor += 1;
            } else {
                if eswn[cursor] == 'n' {
                    tile_at.1 += 1;
                } else if eswn[cursor] == 's' {
                    tile_at.1 -= 1;
                }
                if eswn[cursor + 1] == 'e' {
                    tile_at.0 += 1
                } else if eswn[cursor + 1] == 'w' {
                    tile_at.0 -= 1
                }
                cursor += 2;
            }
        }
        *tiles_hit.entry(tile_at).or_insert(0) += 1;
    }

    let mut black_tiles: HashSet<(isize, isize)> = HashSet::new();
    for (tile, hit_count) in tiles_hit.iter() {
        if hit_count % 2 == 1 {
            black_tiles.insert(*tile);
        }
    }

    if arg == "2" {
        for _ in 1..=100 {
            let mut to_insert = vec![];
            let mut to_remove = vec![];
            let mut checked = HashSet::new();

            for tile in black_tiles.clone() {
                for to_check in neighbours(tile, true) {
                    if !checked.contains(&to_check) {
                        checked.insert(to_check);
                        let nb_black = (&neighbours(to_check, false) & &black_tiles).len();
                        if black_tiles.contains(&to_check) && (nb_black == 0 || nb_black > 2) {
                            to_remove.push(to_check)
                        } else if !black_tiles.contains(&to_check) && nb_black == 2 {
                            to_insert.push(to_check)
                        }
                    }
                }
            }

            for tile in to_insert {
                black_tiles.insert(tile);
            }
            for tile in to_remove {
                black_tiles.remove(&tile);
            }
        }
    }

    println!("{}", black_tiles.len());
}
