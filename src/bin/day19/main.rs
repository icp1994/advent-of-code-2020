use itertools::Itertools;
use std::collections::{HashMap, HashSet};
use std::{env, fs};

fn main() {
    let arg = env::args().nth(1).unwrap();
    if arg != "1" && arg != "2" {
        println!("Only two puzzles daily!");
        return;
    }

    let records: Vec<String> = fs::read_to_string("./src/bin/day19/input")
        .unwrap()
        .split("\n\n")
        .map(|x| x.to_owned())
        .collect();

    let mut generated: HashMap<&str, HashSet<String>> = HashMap::new();
    let mut non_terminals: Vec<&str> = vec![];
    let mut assigned: HashSet<String> = HashSet::new();
    let mut cache: HashMap<&str, HashSet<String>> = HashMap::new();
    let mut rules: HashMap<&str, &str> = HashMap::new();
    let mut variables: Vec<&str> = vec![];

    for (variable, rhs) in records[0]
        .split('\n')
        .map(|e| e.split(": ").collect_tuple().unwrap())
    {
        rules.insert(variable, rhs);
        variables.push(variable);
    }

    'outer: while !variables.is_empty() {
        let mut shift = 0usize;
        for (idx, variable) in variables.clone().iter().enumerate() {
            let rhs = rules[variable];
            let mut flag = false;
            if &rhs[0..1] == "\"" {
                generated.insert(variable, [rhs[1..2].to_owned()].iter().cloned().collect());
                flag = true;
            } else {
                cache.entry(&variable).or_insert_with(|| {
                    rhs.replace(" |", "")
                        .split(' ')
                        .map(|e| e.to_owned())
                        .collect()
                });
                if cache[variable].is_subset(&assigned) {
                    non_terminals.push(variable);
                    flag = true;
                }
            }
            if flag {
                if *variable == "42" {
                    break 'outer;
                }
                assigned.insert(variable.to_string());
                variables.remove(idx - shift);
                shift += 1;
            }
        }
    }

    for nt in &non_terminals {
        let mut strings: HashSet<String> = HashSet::new();
        for product in rules[nt].split(" | ") {
            strings = &strings
                | &product
                    .split(' ')
                    .map(|e| &generated[e])
                    .multi_cartesian_product()
                    .map(|e| e.iter().cloned().join(""))
                    .collect();
        }
        generated.insert(nt, strings);
    }

    let size_42 = generated["42"].iter().next().unwrap().len();
    let size_31 = generated["31"].iter().next().unwrap().len();
    let mut is_generated = 0;

    for test in records[1].split('\n') {
        let mut four_twos = 0;
        let mut three_ones = 0;
        let mut idx = 0;
        let mut switch = false;

        while idx < test.len() {
            if !switch {
                if generated["42"].contains(&test[idx..idx + size_42]) {
                    four_twos += 1;
                    idx += size_42;
                } else {
                    switch = true;
                }
            }
            if switch {
                if generated["31"].contains(&test[idx..idx + size_31]) {
                    three_ones += 1;
                    idx += size_31;
                } else {
                    break;
                }
            }
        }

        if idx == test.len() {
            if arg == "1" && four_twos == 2 && three_ones == 1 {
                is_generated += 1;
            }
            if arg == "2" && four_twos > three_ones && three_ones > 0 {
                is_generated += 1;
            }
        }
    }
    println!("{}", is_generated)
}
