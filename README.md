For problem y (1 or 2) of day x (01, 23, etc.)
```
cargo run --release --bin day{x} -- {y}
```